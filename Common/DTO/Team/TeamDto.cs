﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Common.DTO.Team
{
    public sealed class TeamDto
    {
        [Required]
        public int Id { get; set; }
        [Required]
        [MaxLength(64)]
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }
        public override string ToString()
        {
            return $"Id -- {Id}\n Name -- {Name}\n CreatedAt -- {CreatedAt}\n ";
        }
    }
}
