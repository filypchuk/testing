﻿using AutoMapper;
using BusinessLogicLayer.Interfaces;
using BusinessLogicLayer.MappingProfiles;
using BusinessLogicLayer.Services;
using Common.DTO.Task;
using Common.Enums;
using DataAccessLayer.Context;
using DataAccessLayer.Entities;
using DataAccessLayer.Repositories;
using DataAccessLayer.Repositories.Interfaces;
using System;
using System.Linq;
using Xunit;

namespace BusinessLogicLayer.UnitTests
{
    public class TaskServiceTests : IDisposable
    {
        readonly ITaskService _service;
        readonly IMapper _mapper;
        readonly ITaskRepository _repository;
        readonly InMemoryDBContext _inMemoryDB;
        readonly ProjectDbContext _context;
        public TaskServiceTests()
        {           
            _mapper = MapperConfigurations();
            _inMemoryDB = new InMemoryDBContext();
            _context = _inMemoryDB.GetEmptyDbContext();
            _repository = new TaskRepository(_context);
            _service = new TaskService(_repository, _mapper);
        }

        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        [InlineData(3)]
        public void TasksByUser_GetAllTasks_ShortestsThan45Characters_ByUserId(int userId)
        {
            using (var context = _inMemoryDB.GetDbContext())
            {
                var res = _service.TasksByUser(userId);
                Assert.True(res.All(t => t.Name.Length < 45));
            }
        }
        [Fact]
        public void TasksByUser_GetAllTasks_ShortestsThan45Characters_AddTwoTask_ButWillAdded_OnlyOne()
        {
            using (var context = _inMemoryDB.GetDbContext())
            {
                int userId = 1;
                var expected = _service.TasksByUser(userId).Count();
                var task1 = new TaskEntity { Name = "Very loooooooong name, the longest than 45 characters ", PerformerId = userId, ProjectId = 1 };
                var task2 = new TaskEntity { Name = "short name ", PerformerId = userId, ProjectId = 1 };
                context.Tasks.AddRange(task1, task2);
                context.SaveChanges();
                var actual = _service.TasksByUser(userId).Count();
                Assert.Equal(expected + 1, actual);
            }
        }
        [Fact]
        public void TasksFinishedAt_ThisYear_ByUser_AddTask()
        {
            using (var context = _inMemoryDB.GetDbContext())
            {
                int userId = 1;
                var before = _service.TasksFinishedByUser(userId).ToList().Count;
                var task = new TaskEntity { Name = "short name ", PerformerId = userId, ProjectId = 1, FinishedAt = new DateTime(DateTime.Now.Year, 1, 1) };
                context.Tasks.Add(task);
                context.SaveChanges();
                var after = _service.TasksFinishedByUser(userId).ToList();
                Assert.NotEmpty(after);
                Assert.Equal(before + 1, after.Count);
            }
        }
        public void Dispose()
        {
            
        }
        public IMapper MapperConfigurations()
        {
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new ProjectProfile());
                mc.AddProfile(new TaskProfile());
                mc.AddProfile(new TeamProfile());
                mc.AddProfile(new UserProfile());
            });
            return mappingConfig.CreateMapper();
        }
    }
}
