﻿using Common.DTO.DtoForSevenLinqMethods;
using Common.DTO.Project;
using ConsoleClient.HttpRequests;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleClient.ClientService
{
    public class ProjectClientService
    {
        private readonly HttpRequest _request;
        public ProjectClientService()
        {
            _request = new HttpRequest();
        }
        public IEnumerable<ProjectDto> GetAll()
        {
           return _request.GetAll<ProjectDto>("projects");
        }
        public ProjectDto GetById(int id)
        {
           return _request.GetById<ProjectDto>("projects", id);
        }
        public ProjectDto Create(ProjectCreateDto createDto)
        {
            var json = JsonConvert.SerializeObject(createDto);
            return _request.Create<ProjectDto>("projects", json);
        }
        public ProjectDto Update(ProjectDto updateDto)
        {
            var json = JsonConvert.SerializeObject(updateDto);
            return _request.Update<ProjectDto>("projects", json);
        }
        public void Delete(int id)
        {
            _request.Delete("projects", id);
        }
        public Dictionary<ProjectDto, int> CountTasksInProjectByUser(int id)
        {
           var p = _request.GetById<IEnumerable<ProjectAndCountTasksDto>>("projects/tasks-in-project", id);
           return p.ToDictionary(p => p.Key, p => p.Value);
        }
        public IEnumerable<ProjectAndTwoTasksDto> AllProjectsWithTheLongestTaskAndTheShortest()
        {
            return _request.GetAll<ProjectAndTwoTasksDto>("projects/all-projects-with");
        }
    }
}
