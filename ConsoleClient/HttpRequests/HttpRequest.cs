﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace ConsoleClient.HttpRequests
{
    public class HttpRequest
    {
        private const string URL = "https://localhost:5001/api";
        private readonly HttpClient _client;
        public HttpRequest()
        {
            _client = new HttpClient();
        }
        public IEnumerable<T> GetAll<T>(string partUrl)
        {
            var response = _client.GetAsync($"{URL}/{partUrl}");
            Console.WriteLine($"--------|GET: {URL}/{partUrl} |--------");
            Console.WriteLine(response.Result);
            Console.WriteLine("---------------------------------------------------------------------");
            string json = response.Result.Content.ReadAsStringAsync().Result;
            var list = JsonConvert.DeserializeObject<List<T>>(json);
            return list;
        }
        public T GetById<T>(string partUrl, int id)
        {
            var response = _client.GetAsync($"{URL}/{partUrl}/{id}");
            Console.WriteLine($"--------|GET: {URL}/{partUrl}/{id} |--------");
            Console.WriteLine(response.Result);
            Console.WriteLine("---------------------------------------------------------------------");

            string json = response.Result.Content.ReadAsStringAsync().Result;
            var dto = JsonConvert.DeserializeObject<T>(json);
            return dto;
        }
        public T Create<T>(string partUrl, string jsonObj)
        {
            HttpContent stringContent = new StringContent(jsonObj, Encoding.UTF8, "application/json");
            var response = _client.PostAsync($"{URL}/{partUrl}", stringContent);
            Console.WriteLine($"--------|POST: {URL}/{partUrl} |--------");
            Console.WriteLine($"--------|JSON: {jsonObj}|--------");
            Console.WriteLine(response.Result);
            Console.WriteLine("---------------------------------------------------------------------");
            string json = response.Result.Content.ReadAsStringAsync().Result;
            var dto = JsonConvert.DeserializeObject<T>(json);
            return dto;
        }
        public T Update<T>(string partUrl, string jsonObj)
        {
            HttpContent stringContent = new StringContent(jsonObj, Encoding.UTF8, "application/json");
            var response = _client.PutAsync($"{URL}/{partUrl}", stringContent);
            Console.WriteLine($"--------|PUT: {URL}/{partUrl} |--------");
            Console.WriteLine($"--------|JSON: {jsonObj}|--------");
            Console.WriteLine(response.Result);
            Console.WriteLine("---------------------------------------------------------------------");
            string json = response.Result.Content.ReadAsStringAsync().Result;
            var dto = JsonConvert.DeserializeObject<T>(json);
            return dto;
        }
        public void Delete(string partUrl, int id)
        {
            var response = _client.DeleteAsync($"{URL}/{partUrl}/{id}");
            Console.WriteLine($"--------|DELETE: {URL}/{partUrl}/{id} |--------");
            Console.WriteLine(response.Result.StatusCode);
            Console.WriteLine("---------------------------------------------------------------------");
        }
    }
}
