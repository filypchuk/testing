﻿using Common.DTO.DtoForSevenLinqMethods;
using Common.DTO.Project;
using System.Collections.Generic;

namespace BusinessLogicLayer.Interfaces
{
    public interface IProjectService
    {
        public IEnumerable<ProjectDto> GetAll();
        public ProjectDto GetById(int id);
        public ProjectDto Create(ProjectCreateDto dto);
        public void Update(ProjectDto dto);
        public void Delete(int id);
        public IEnumerable<ProjectAndCountTasksDto> TasksInProjectByUser(int userId);
        public IEnumerable<ProjectAndTwoTasksDto> AllProjectsWithTheLongestTaskAndTheShortest();
        public IEnumerable<NotFinishedTasksByUserIdDto> AllNotFinishedTasksForEveryProject(int userId);
    }
}
