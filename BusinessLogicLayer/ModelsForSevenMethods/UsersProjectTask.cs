﻿using DataAccessLayer.Entities;

namespace BusinessLogicLayer.ModelsForSevenMethods
{
    public sealed class UsersProjectTask
    {
        public User User { get; set; }
        public Project LastProject { get; set; }
        public int CountTasksByLastProject { get; set; }
        public int CountStartCancelTasks { get; set; }
        public TaskEntity TheLongestByTimeTask { get; set; }
    }
}
