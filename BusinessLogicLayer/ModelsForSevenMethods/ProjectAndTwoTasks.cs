﻿using DataAccessLayer.Entities;

namespace BusinessLogicLayer.ModelsForSevenMethods
{
    public sealed class ProjectAndTwoTasks
    {
        public Project Project { get; set; }
        public TaskEntity TheLongestTask { get; set; }
        public TaskEntity TheShortestTask { get; set; }
        public int CountUsersInTeam { get; set; }
    }
}
