﻿using Common.DTO.DtoForSevenLinqMethods;
using Common.DTO.User;
using System.Collections.Generic;

namespace BusinessLogicLayer.Interfaces
{
    public interface IUserService
    {
        public IEnumerable<UserDto> GetAll();
        public UserDto GetById(int id);
        public UserDto Create(UserCreateDto dto);
        public void Update(UserDto dto);
        public void Delete(int id);
        public IEnumerable<UserByAlphabetAndTasksDto> UserByAlphabetAndTasks();
        public UsersProjectTaskDto UsersLastProjectAndTasks(int userId);
    }
}
