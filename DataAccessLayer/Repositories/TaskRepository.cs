﻿using DataAccessLayer.Context;
using DataAccessLayer.Entities;
using DataAccessLayer.Repositories.Interfaces;
using System.Collections.Generic;

namespace DataAccessLayer.Repositories
{
    public class TaskRepository : ITaskRepository
    {
        private protected readonly ProjectDbContext _context;

        public TaskRepository(ProjectDbContext context)
        {
            _context = context;
        }

        public IEnumerable<TaskEntity> GetAll()
        {
            return _context.Tasks;
        }

        public TaskEntity GetById(int id)
        {
            return _context.Tasks.Find(id);
        }

        public TaskEntity Create(TaskEntity entity)
        {
            _context.Tasks.Add(entity);
            return entity;
        }

        public void Update(TaskEntity entity)
        {
           _context.Tasks.Update(entity);
        }

        public void Delete(int id)
        {
            TaskEntity entity = _context.Tasks.Find(id);
            if (entity != null)
                _context.Tasks.Remove(entity);
        }
        public void Save()
        {
            _context.SaveChanges();
        }
    }
}
