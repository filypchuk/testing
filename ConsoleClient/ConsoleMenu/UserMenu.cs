﻿using ConsoleClient.Helpers;
using ConsoleClient.View;
using System;

namespace ConsoleClient.ConsoleMenu
{
    public class UserMenu
    {
        private readonly PrintToConsole printToConsole;
        private readonly Printing<string, Color> print;
        private readonly UserView view;
        public UserMenu()
        {
            printToConsole = new PrintToConsole();
            print = printToConsole.PrintColor;
            view = new UserView();
        }
        private void DisplayUserMenu()
        {
            Console.Clear();
            print("Click button 1-7 to select\n", Color.Red);
            print("1---Get all User \n", Color.Green);
            print("2---Get User by id \n", Color.Green);
            print("3---Create User \n", Color.Blue);
            print("4---Update User \n", Color.Yellow);
            print("5---Delete User \n", Color.Red);
            print("6---Get a list of users in alphabetical order with tasks\n", Color.Green);
            print("7---User, last user project,the total number of tasks under the last project...\n", Color.Green);
            print("\n   Click Esc to back menu", Color.Red);
        }
        public void StartUserMenu()
        {
            DisplayUserMenu();
            while (true)
            {
                var keyInfo = Console.ReadKey(true);
                switch (keyInfo.Key)
                {
                    case ConsoleKey.D1:
                        view.AllUsers();
                        break;
                    case ConsoleKey.D2:
                        view.UserById();
                        break;
                    case ConsoleKey.D3:
                        view.CreateUser();
                        break;
                    case ConsoleKey.D4:
                        view.UpdateUser();
                        break;
                    case ConsoleKey.D5:
                        view.Delete();
                        break;
                    case ConsoleKey.D6:
                        view.UserByAlphabet();
                        break;
                    case ConsoleKey.D7:
                        view.UserLastProject();
                        break;
                    case ConsoleKey.Escape:
                        return;
                    default:
                        DisplayUserMenu();
                        break;
                }
                DisplayUserMenu();
            }
        }
    }
}
