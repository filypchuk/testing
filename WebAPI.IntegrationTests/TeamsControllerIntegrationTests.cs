﻿using Common.DTO.DtoForSevenLinqMethods;
using Common.DTO.Team;
using Common.DTO.User;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;


namespace WebAPI.IntegrationTests
{
    public class TeamsControllerIntegrationTests : IClassFixture<CustomWebApplicationFactory<Startup>>, IDisposable
    {
        private readonly HttpClient client;
        public TeamsControllerIntegrationTests(CustomWebApplicationFactory<Startup> factory)
        {
            client = factory.CreateClient();
        }
        public void Dispose()
        {
        }
        [Fact]
        public async Task CreateTeam_ShouldWork()
        {
            var createDto = new TeamCreateDto { Name = "Team1",};

            var json = JsonConvert.SerializeObject(createDto);
            HttpContent stringContent = new StringContent(json, Encoding.UTF8, "application/json");
            var response = await client.PostAsync("api/teams", stringContent);
            var stringResponse = await response.Content.ReadAsStringAsync();
            var responseDto = JsonConvert.DeserializeObject<TeamDto>(stringResponse);

            Assert.Equal(HttpStatusCode.Created, response.StatusCode);
            Assert.Equal(createDto.Name, responseDto.Name);
            Assert.Equal(DateTime.Now.Day, responseDto.CreatedAt.Day);
        }
        [Fact]
        public async Task TeamAndUsers_GetAllTeams_With_Users_ShouldWork()
        {
            var response = await client.GetAsync("api/teams/team-and-users");
            var stringResponse = await response.Content.ReadAsStringAsync();
            var listTeamAndUsers = JsonConvert.DeserializeObject<List<TeamAndUsersDto>>(stringResponse);

            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            Assert.True(listTeamAndUsers.All(t=>t.Users.All(u=>u.TeamId == t.Id)));
        }
        [Fact]
        public async Task TeamAndUsers_CreateNewTeamAndUsers_ShouldWork()
        {
            var createTeam = new TeamDto { Name = Guid.NewGuid().ToString(), };
            var cratedTeamResponse = await client.PostAsync("api/teams", new StringContent(JsonConvert.SerializeObject(createTeam), Encoding.UTF8, "application/json"));

            var stringCratedTeamResponse = await cratedTeamResponse.Content.ReadAsStringAsync();
            var createdTeamDto = JsonConvert.DeserializeObject<TeamDto>(stringCratedTeamResponse);

            //Add two users to new team
            var createUser1 = new UserCreateDto { FirstName = "FirstName1", LastName = "LastName1", TeamId = createdTeamDto.Id };
            var createUser2 = new UserCreateDto { FirstName = "FirstName2", LastName = "LastName2", TeamId = createdTeamDto.Id };
            await client.PostAsync("api/users", new StringContent(JsonConvert.SerializeObject(createUser1), Encoding.UTF8, "application/json"));
            await client.PostAsync("api/users", new StringContent(JsonConvert.SerializeObject(createUser2), Encoding.UTF8, "application/json"));

            var response = await client.GetAsync("api/teams/team-and-users");
            var stringResponse = await response.Content.ReadAsStringAsync();
            var listTeamAndUsers = JsonConvert.DeserializeObject<List<TeamAndUsersDto>>(stringResponse);

            var teamAndUsersItem = listTeamAndUsers.FirstOrDefault(x => x.Name == createTeam.Name);

            Assert.Equal(2, teamAndUsersItem.Users.Count());
            Assert.NotNull(teamAndUsersItem);
            Assert.True(teamAndUsersItem.Users.All(user => user.TeamId == createdTeamDto.Id));
        }
    }
}
