﻿using DataAccessLayer.Context;
using DataAccessLayer.Entities;
using DataAccessLayer.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace DataAccessLayer.Repositories
{
    public class UserRepository : IUserRepository
    {
        private protected readonly ProjectDbContext _context;

        public UserRepository(ProjectDbContext context)
        {
            _context = context;
        }

        public IEnumerable<User> GetAll()
        {
            return _context.Set<User>()
                .Include(u => u.Projects)
                    .ThenInclude(p => p.Tasks)
                .Include(u => u.Tasks);
        }

        public User GetById(int id)
        {
            return _context.Users.Find(id);
        }

        public User Create(User entity)
        {
            _context.Users.Add(entity);
            return entity;
        }

        public void Update(User entity)
        {
            _context.Users.Update(entity);
        }

        public void Delete(int id)
        {
            User entity = _context.Users.Find(id);
            if (entity != null)
                _context.Users.Remove(entity);
        }
        public void Save()
        {
            _context.SaveChanges();
        }
    }
}
