﻿using System.ComponentModel.DataAnnotations;

namespace Common.DTO.Team
{
    public sealed class TeamCreateDto
    {
        [Required]
        [MaxLength(64)]
        public string Name { get; set; }
    }
}
