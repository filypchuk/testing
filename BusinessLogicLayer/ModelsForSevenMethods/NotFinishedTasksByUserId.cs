﻿using DataAccessLayer.Entities;
using System.Collections.Generic;

namespace BusinessLogicLayer.ModelsForSevenMethods
{
    public class NotFinishedTasksByUserId
    {
        public Project Project { get; set; }
        public IEnumerable<TaskEntity> Tasks { get; set; }
    }
}
