﻿using Common.DTO.DtoForSevenLinqMethods;
using Common.DTO.Task;
using System.Collections.Generic;

namespace BusinessLogicLayer.Interfaces
{
    public interface ITaskService
    {
        public IEnumerable<TaskDto> GetAll();
        public TaskDto GetById(int id);
        public TaskDto Create(TaskCreateDto dto);
        public void Update(TaskDto dto);
        public void Delete(int id);
        public IEnumerable<TaskDto> TasksByUser(int userId);
        public IEnumerable<TasksFinishedByUserDto> TasksFinishedByUser(int userId);
    }
}
