﻿using AutoMapper;
using BusinessLogicLayer.Interfaces;
using BusinessLogicLayer.Services.Abstract;
using Common.DTO.DtoForSevenLinqMethods;
using Common.DTO.User;
using BusinessLogicLayer.ModelsForSevenMethods;
using DataAccessLayer.Entities;
using Common.Enums;
using DataAccessLayer.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BusinessLogicLayer.Services
{
    public class UserService : BaseService, IUserService
    {
        private readonly IUserRepository _userRepository;

        public UserService(IUserRepository userRepository, IMapper mapper) : base( mapper) 
        {
            _userRepository = userRepository;
        }
        public IEnumerable<UserDto> GetAll()
        {
            return _mapper.Map<IEnumerable<UserDto>>(_userRepository.GetAll());
        }

        public UserDto GetById(int id)
        {
            return _mapper.Map<UserDto>(_userRepository.GetById(id));
        }
        public UserDto Create(UserCreateDto dto)
        {
            var newUser = _mapper.Map<User>(dto);
            newUser.RegisteredAt = DateTime.Now;
            _userRepository.Create(newUser);
            _userRepository.Save();
            return _mapper.Map<UserDto>(newUser); 
        }

        public void Update(UserDto dto)
        {
            var entityUpdate = _mapper.Map<User>(dto);
            _userRepository.Update(entityUpdate);
            _userRepository.Save();
        }
        public void Delete(int id)
        {
            _userRepository.Delete(id);
            _userRepository.Save();
        }
        public IEnumerable<UserByAlphabetAndTasksDto> UserByAlphabetAndTasks()
        {
            var userByAlphabet = _userRepository.GetAll()
                .Select(user => 
                  new UserByAlphabetAndTasks
                  {
                      User = user,
                      Tasks = user.Tasks.OrderByDescending(t => t.Name.Length)
                  })
                  .OrderBy(u => u.User.FirstName);
            return _mapper.Map<IEnumerable<UserByAlphabetAndTasksDto>>(userByAlphabet);
        }
        public UsersProjectTaskDto UsersLastProjectAndTasks(int userId)
        {
            var usersProjectTask = _userRepository.GetAll().Where(x => x.Id == userId)
                .Select(user =>
                    new UsersProjectTask
                    {
                        User = user,
                        LastProject = user.Projects.OrderBy(p => p.CreatedAt).LastOrDefault(),
                        CountTasksByLastProject = user.Projects.OrderBy(p => p.CreatedAt).LastOrDefault()?.Tasks.Count ?? 0,
                        CountStartCancelTasks = user.Tasks.Where(t => t.State == TaskStates.Canceled || t.State == TaskStates.Started).Count() ,
                        TheLongestByTimeTask = user.Tasks.OrderBy(t => t.FinishedAt - t.CreatedAt).ToList().LastOrDefault()

                    }).ToList().LastOrDefault();

            return  _mapper.Map<UsersProjectTaskDto>(usersProjectTask);
        }
    }
}
