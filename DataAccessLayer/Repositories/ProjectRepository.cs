﻿using DataAccessLayer.Context;
using DataAccessLayer.Entities;
using DataAccessLayer.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace DataAccessLayer.Repositories
{
    public class ProjectRepository : IProjectRepository
    {
        private readonly ProjectDbContext _context;

        public ProjectRepository(ProjectDbContext context)
        {
            _context = context;
        }

        public IEnumerable<Project> GetAll()
        {
            return _context.Projects
                .Include(p => p.Tasks)
                    .ThenInclude(t=>t.User)
                .Include(p => p.Team)
                    .ThenInclude(t => t.Users)
                .Include(p => p.User);
        }

        public Project GetById(int id)
        {
            return _context.Projects.Find(id);
        }

        public Project Create(Project entity)
        {
            _context.Projects.Add(entity);
            return entity;
        }

        public void Update(Project entity)
        {
            _context.Projects.Update(entity);

        }

        public void Delete(int id)
        {
            Project entity = _context.Projects.Find(id);
            if (entity != null)
                _context.Projects.Remove(entity);
        }

        public void Save()
        {
            _context.SaveChanges();
        }
    }
}
