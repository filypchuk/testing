﻿using Common.DTO.User;
using ConsoleClient.ClientService;
using ConsoleClient.Helpers;
using System;

namespace ConsoleClient.View
{
    public class UserView
    {
        private readonly UserClientService _service;
        private readonly PrintToConsole printToConsole;
        private readonly Printing<string, Color> print;
        private readonly WaitingForEnter printWaitingForEnter;
        private readonly CheckInput checkInput;
        private readonly CheckOutput checkOutput;
        public UserView()
        {
            printToConsole = new PrintToConsole();
            print = printToConsole.PrintColor;
            printWaitingForEnter = printToConsole.WaitEnter;
            checkInput = new CheckInput();
            checkOutput = new CheckOutput();
            _service = new UserClientService();
        }
        public void AllUsers()
        {
            Console.Clear();
            print("Get all users", Color.Yellow);
            var listDto = _service.GetAll();
            if (!checkOutput.EmptyList(listDto))
            {
                foreach (var dto in listDto)
                {
                    print(dto.ToString(), Color.Yellow);
                }
            }
            printWaitingForEnter();
        }
        public void UserById()
        {
            Console.Clear();
            print("Get user by id", Color.Yellow);
            print("Enter user id", Color.Green);
            int id = checkInput.CheckingInt();
            var dto = _service.GetById(id);
            if (!checkOutput.NoContent(dto))
            {
                print(dto.ToString(), Color.Yellow);
            }
            printWaitingForEnter();
        }
        public void CreateUser()
        {
            Console.Clear();
            print("Create user", Color.Yellow);
            UserCreateDto createDto = new UserCreateDto();
            print("Enter FirstName", Color.Green);
            createDto.FirstName = checkInput.CheckingString();
            print("Enter LastName", Color.Green);
            createDto.LastName = checkInput.CheckingString();
            print("Enter Email", Color.Green);
            createDto.Email = checkInput.CheckingString();
            print("Enter Birthday", Color.Green);
            createDto.Birthday = checkInput.CheckingDateTime();
            print("Enter TeamId", Color.Green);
            createDto.TeamId = checkInput.CheckingInt();

            var dto = _service.Create(createDto);
            if (!checkOutput.NoContent(dto))
            {
                print(dto.ToString(), Color.Yellow);
            }
            printWaitingForEnter();
        }
        public void UpdateUser()
        {
            Console.Clear();
            print("Update project", Color.Yellow);
            print("Enter project id", Color.Green);
            int id = checkInput.CheckingInt();
            var dto = _service.GetById(id);
            if (!checkOutput.NoContent(dto))
            {
                print(dto.ToString(), Color.Yellow);
                UserDto updateDto = new UserDto();
                updateDto.Id = dto.Id;
                print("Enter FirstName", Color.Green);
                updateDto.FirstName = checkInput.CheckingString();
                print("Enter LastName", Color.Green);
                updateDto.LastName = checkInput.CheckingString();
                print("Enter Email", Color.Green);
                updateDto.Email = checkInput.CheckingString();
                print("Enter Deadline", Color.Green);
                updateDto.Birthday = checkInput.CheckingDateTime();
                print("Enter TeamId", Color.Green);
                updateDto.TeamId = checkInput.CheckingInt();
                updateDto.RegisteredAt = dto.RegisteredAt;

                var responseDto = _service.Update(updateDto);
                print("Updated dto", Color.Green);
                if (!checkOutput.NoContent(responseDto))
                {
                    print(responseDto.ToString(), Color.Yellow);
                }
            }
            printWaitingForEnter();
        }
        public void Delete()
        {
            Console.Clear();
            print("Delete user", Color.Yellow);
            print("Enter user id", Color.Green);
            int id = checkInput.CheckingInt();
            _service.Delete(id);
            printWaitingForEnter();
        }
        public void UserByAlphabet()
        {
            Console.Clear();
            print("Get a list of users in alphabetical order first_name (ascending) " +
                "with sorted tasks by length name (descending).", Color.None);
            var list = _service.UserByAlphabet();
            if (!checkOutput.EmptyList(list))
            {
                foreach (var i in list)
                {
                    print("User", Color.None);
                    print(i.User.ToString(), Color.Green);

                    print("Tasks", Color.Yellow);
                    if (!checkOutput.EmptyList(list))
                    {
                        foreach (var task in i.Tasks)
                        {
                            print(task.ToString(), Color.Yellow);
                        }
                    }
                    else print("User doesn't have tasks or something else", Color.Red);
                    print("-------------------------------", Color.Blue);
                }
            }
            else print("Users not found or something else", Color.Red);
            printWaitingForEnter();
        }
        public void UserLastProject()
        {
            Console.Clear();
            print("User, last user project,the total number of tasks under the last project, \n " +
                "the total number of incomplete or canceled tasks for the user, \n" +
                "the user's longest task by date (earliest created - latest completed)", Color.Yellow);
            print("Enter user id", Color.Green);
            int userId = checkInput.CheckingInt();
            var result = _service.UserLastProject(userId);

            if (!checkOutput.NoContent(result?.User))
            {
                print("User", Color.Yellow);
                print(result.User.ToString(), Color.Green);
                if (!checkOutput.NoContent(result.LastProject))
                {
                    print("Last user project (by creation date)", Color.Yellow);
                    print(result.LastProject.ToString(), Color.Green);
                }
                else print("User doesn't have any projcet or something else", Color.Red);
                print("The total number of tasks under the last project", Color.Yellow);
                print(result.CountTasksByLastProject.ToString(), Color.Green);
                print("The total number of started or canceled tasks for the user", Color.Yellow);
                print(result.CountStartCancelTasks.ToString(), Color.Green);

                if (!checkOutput.NoContent(result.TheLongestByTimeTask))
                {
                    print("The user's longest task by date (earliest created - latest completed)", Color.Yellow);
                    print(result.TheLongestByTimeTask.ToString(), Color.Green);
                }
                else print("User doesn't have any tasks or something else", Color.Red);
            }
            else print("User not found or something else", Color.Red);
            print("-------------------------------", Color.Blue);
            printWaitingForEnter();
        }
    }
}
