﻿using Common.DTO.DtoForSevenLinqMethods;
using Common.DTO.Team;
using ConsoleClient.HttpRequests;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace ConsoleClient.ClientService
{
    public class TeamClientService
    {
        private readonly HttpRequest _request;
        public TeamClientService()
        {
            _request = new HttpRequest();
        }
        public IEnumerable<TeamDto> GetAll()
        {
            return _request.GetAll<TeamDto>("teams");
        }
        public TeamDto GetById(int id)
        {
            return _request.GetById<TeamDto>("teams", id);
        }
        public TeamDto Create(TeamCreateDto createDto)
        {
            var json = JsonConvert.SerializeObject(createDto);
            return _request.Create<TeamDto>("teams", json);
        }
        public TeamDto Update(TeamDto updateDto)
        {
            var json = JsonConvert.SerializeObject(updateDto);
            return _request.Update<TeamDto>("teams", json);
        }
        public void Delete(int id)
        {
            _request.Delete("teams", id);
        }
        public IEnumerable<TeamAndUsersDto> TeamAndUsers()
        {
            return _request.GetAll<TeamAndUsersDto>("teams/team-and-users");
        }
    }
}
