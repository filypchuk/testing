﻿using System.Collections.Generic;
using BusinessLogicLayer.Interfaces;
using Common.DTO.DtoForSevenLinqMethods;
using Common.DTO.Team;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamsController : ControllerBase
    {
        private readonly ITeamService _teamService;

        public TeamsController(ITeamService teamService)
        {
            _teamService = teamService;
        }

        [HttpGet]
        public IEnumerable<TeamDto> GetAll()
        {
            return _teamService.GetAll();
        }

        [HttpGet("{id}")]
        public ActionResult<TeamDto> Get(int id)
        {

            var dto = _teamService.GetById(id);
            if (dto != null)
                return Ok(dto);
            return NotFound();
        }

        [HttpPost]
        public ActionResult<TeamDto> Post([FromBody] TeamCreateDto createDto)
        {
            var dto = _teamService.Create(createDto);
            return Created($"{dto.Id}", dto);
        }

        [HttpPut]
        public ActionResult<TeamDto> Put([FromBody] TeamDto updateDto)
        {
            var dto = _teamService.GetById(updateDto.Id);
            if (dto != null)
            {
                _teamService.Update(updateDto);
                return Ok(dto);
            }
            return NotFound();

        }

        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            var dto = _teamService.GetById(id);
            if (dto == null)
                return NotFound();
            _teamService.Delete(id);
            return NoContent();
        }
        [HttpGet("team-and-users")]
        public IEnumerable<TeamAndUsersDto> TeamAndUsers()
        {
            return _teamService.TeamAndUsersOlderTenYears();
        }
    }
}
