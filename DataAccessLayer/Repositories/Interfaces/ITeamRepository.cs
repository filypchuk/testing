﻿using DataAccessLayer.Entities;
using System;

namespace DataAccessLayer.Repositories.Interfaces
{
   public interface ITeamRepository : IRepository<Team>   {  }
}
