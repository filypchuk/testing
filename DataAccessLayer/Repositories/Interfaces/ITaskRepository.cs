﻿using DataAccessLayer.Entities;

namespace DataAccessLayer.Repositories.Interfaces
{
    public interface ITaskRepository : IRepository<TaskEntity>
    {
    }
}
