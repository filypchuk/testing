﻿using System.Collections.Generic;
using BusinessLogicLayer.Interfaces;
using Common.DTO.DtoForSevenLinqMethods;
using Common.DTO.Project;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        private readonly IProjectService _projectService;

        public ProjectsController(IProjectService projectService)
        {
            _projectService = projectService;
        }

        [HttpGet]
        public IEnumerable<ProjectDto> GetAll()
        {
            return _projectService.GetAll();
        }

        [HttpGet("{id}")]
        public ActionResult<ProjectDto> Get(int id)
        {

            var dto = _projectService.GetById(id);
            if(dto != null)
                return Ok(dto);
            return NotFound();
        }
     
        [HttpPost]
        public ActionResult<ProjectDto> Post([FromBody] ProjectCreateDto createDto)
        {
            var dto = _projectService.Create(createDto);
            return Created($"{dto.Id}", dto);
        }

        [HttpPut]
        public ActionResult<ProjectDto> Put([FromBody] ProjectDto updateDto)
        {
            
            var dto = _projectService.GetById(updateDto.Id);
            if (dto != null) 
            { 
                _projectService.Update(updateDto);
                return Ok(dto);
            }
            return NotFound();
      

        }

        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            var dto = _projectService.GetById(id);
            if (dto == null)                
                return NotFound();
            _projectService.Delete(id);
            return NoContent();
        }

        [HttpGet("tasks-in-project/{id}")]
        public IEnumerable<ProjectAndCountTasksDto> TasksInProjectByUser(int id)
        {
            return _projectService.TasksInProjectByUser(id);
        }
        [HttpGet("all-projects-with")]
        public IEnumerable<ProjectAndTwoTasksDto> AllProjectsWithTheLongestTaskAndTheShortest()
        {
            return _projectService.AllProjectsWithTheLongestTaskAndTheShortest();
        }
        [HttpGet("all-notfinished-tasks-by-user/{id}")]
        public IEnumerable<NotFinishedTasksByUserIdDto> AllNotFinishedTasksForEveryProject(int id)
        {
            return _projectService.AllNotFinishedTasksForEveryProject(id);
        }
    }
}
