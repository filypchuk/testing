﻿using System.Collections.Generic;
using BusinessLogicLayer.Interfaces;
using Common.DTO.DtoForSevenLinqMethods;
using Common.DTO.Task;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        private readonly ITaskService _taskService;

        public TasksController(ITaskService taskService)
        {
            _taskService = taskService;
        }

        [HttpGet]
        public IEnumerable<TaskDto> GetAll()
        {
            return _taskService.GetAll();
        }
  
        [HttpGet("{id}")]
        public ActionResult<TaskDto> Get(int id)
        {

            var dto = _taskService.GetById(id);
            if (dto != null)
                return Ok(dto);
            return NotFound();
        }
      
        [HttpPost]
        public ActionResult<TaskDto> Post([FromBody] TaskCreateDto createDto)
        {
            var dto = _taskService.Create(createDto);
            return Created($"{dto.Id}", dto);
        }

        [HttpPut]
        public ActionResult<TaskDto> Put([FromBody] TaskDto updateDto)
        {
            var dto = _taskService.GetById(updateDto.Id);
            if (dto != null)
            {
                _taskService.Update(updateDto);
                return Ok(dto);
            }
            return NotFound();
        }

        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            var dto = _taskService.GetById(id);
            if (dto == null)
                return NotFound();
            _taskService.Delete(id);
            return NoContent();
        }

        [HttpGet("task-by-user/{id}")]
        public IEnumerable<TaskDto> TasksByUser(int id)
        {
            return _taskService.TasksByUser(id);
        }
        [HttpGet("task-finished-by-user/{id}")]
        public IEnumerable<TasksFinishedByUserDto> TasksFinishedByUser(int id)
        {
            return _taskService.TasksFinishedByUser(id);
        }
    }
}
