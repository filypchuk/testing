﻿using System.Collections.Generic;
using BusinessLogicLayer.Interfaces;
using Common.DTO.DtoForSevenLinqMethods;
using Common.DTO.User;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUserService _userService;

        public UsersController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpGet]
        public IEnumerable<UserDto> GetAll()
        {
            return _userService.GetAll();
        }

        [HttpGet("{id}")]
        public ActionResult<UserDto> Get(int id)
        {

            var dto = _userService.GetById(id);
            if (dto != null)
                return Ok(dto);
            return NotFound();
        }

        [HttpPost]
        public ActionResult<UserDto> Post([FromBody] UserCreateDto createDto)
        {
            var dto = _userService.Create(createDto);
            return Created($"{dto.Id}", dto);
        }

        [HttpPut]
        public ActionResult<UserDto> Put([FromBody] UserDto updateDto)
        {
            var dto = _userService.GetById(updateDto.Id);
            if (dto != null)
            {
                _userService.Update(updateDto);
                return Ok(dto);
            }
            return NotFound();

        }

        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            var dto = _userService.GetById(id);
            if (dto == null)
                return NotFound();
            _userService.Delete(id);
            return NoContent();
        }
        [HttpGet("user-by-alphabet")]
        public IEnumerable<UserByAlphabetAndTasksDto> UserByAlphabet()
        {
            return _userService.UserByAlphabetAndTasks();
        }
        [HttpGet("user-last-project/{id}")]
        public UsersProjectTaskDto UserLastProject(int id)
        {
            return _userService.UsersLastProjectAndTasks(id);
        }
    }
}
