﻿using Common.DTO.DtoForSevenLinqMethods;
using Common.DTO.Task;
using ConsoleClient.HttpRequests;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleClient.ClientService
{
    public class TaskClientService
    {
        private readonly HttpRequest _request;
        public TaskClientService()
        {
            _request = new HttpRequest();
        }
        public IEnumerable<TaskDto> GetAll()
        {
            return _request.GetAll<TaskDto>("tasks");
        }
        public TaskDto GetById(int id)
        {
            return _request.GetById<TaskDto>("tasks", id);
        }
        public TaskDto Create(TaskCreateDto createDto)
        {
            var json = JsonConvert.SerializeObject(createDto);
            return _request.Create<TaskDto>("tasks", json);
        }
        public TaskDto Update(TaskDto updateDto)
        {
            var json = JsonConvert.SerializeObject(updateDto);
            return _request.Update<TaskDto>("tasks", json);
        }
        public void Delete(int id)
        {
            _request.Delete("tasks", id);
        }
        public IEnumerable<TaskDto> TasksByUser(int id)
        {
            return _request.GetById<IEnumerable<TaskDto>>("tasks/task-by-user", id); 
        }
        public IEnumerable<TasksFinishedByUserDto> TasksFinishedByUser(int id)
        {
            return _request.GetById<IEnumerable<TasksFinishedByUserDto>>("tasks/task-finished-by-user", id);
        }
    }
}
