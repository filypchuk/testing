﻿using AutoMapper;
using Common.DTO.Task;
using DataAccessLayer.Entities;

namespace BusinessLogicLayer.MappingProfiles
{
    public class TaskProfile : Profile
    {
        public TaskProfile()
        {
            CreateMap<TaskEntity, TaskDto>();
            
            CreateMap<TaskDto, TaskEntity>().ForMember(t => t.Project, otp=>otp.Ignore()).ForMember(t => t.User, otp => otp.Ignore());
            CreateMap<TaskCreateDto, TaskEntity>(); 
        }
    }
}