﻿using Common.Enums;
using System;
using System.ComponentModel.DataAnnotations;

namespace Common.DTO.Task
{
    public sealed class TaskDto
    {
        //[Required]
        public int Id { get; set; }
       // [Required]
       // [MaxLength(128)]
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime FinishedAt { get; set; }
       // [Required]
        public TaskStates State { get; set; }
       // [Required]
        public int ProjectId { get; set; }
        public int? PerformerId { get; set; }
        public override string ToString()
        {
            return $"Id -- {Id}\n Name -- {Name}\n CreatedAt -- {CreatedAt}\n FinishedAt -- {FinishedAt}\n " +
                $"State -- {State}\n ProjectId -- {ProjectId}\n PerformerId -- {PerformerId}\n Description -- {Description}\n ";
        }
    }
}
