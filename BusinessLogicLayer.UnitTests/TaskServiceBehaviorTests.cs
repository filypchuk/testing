﻿using AutoMapper;
using BusinessLogicLayer.Interfaces;
using BusinessLogicLayer.Services;
using Common.DTO.Task;
using DataAccessLayer.Entities;
using DataAccessLayer.Repositories.Interfaces;
using FakeItEasy;
using Xunit;

namespace BusinessLogicLayer.UnitTests
{
    public class TaskServiceBehaviorTests
    {
        readonly ITaskService _service;
        readonly IMapper _mapper;
        readonly ITaskRepository _repository;
        public TaskServiceBehaviorTests()
        {
            _mapper = A.Fake<IMapper>();
            _repository = A.Fake<ITaskRepository>();
            _service = new TaskService(_repository, _mapper);
        }
        [Fact]
        public void UpdateTask_MustCallOnce()
        {
            _service.Update(new TaskDto());
            A.CallTo(() => _repository.Update(A<TaskEntity>.Ignored)).MustHaveHappenedOnceExactly();
            A.CallTo(() => _repository.Save()).MustHaveHappenedOnceExactly();
        }
    }
}
