﻿using Common.DTO.Project;
using Common.DTO.Task;

namespace Common.DTO.DtoForSevenLinqMethods
{
    public sealed class ProjectAndTwoTasksDto
    {
        public ProjectDto Project { get; set; }
        public TaskDto TheLongestTask { get; set; }
        public TaskDto TheShortestTask { get; set; }
        public int CountUsersInTeam { get; set; }
    }
}
