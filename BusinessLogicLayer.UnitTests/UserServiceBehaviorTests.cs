﻿using AutoMapper;
using BusinessLogicLayer.Interfaces;
using BusinessLogicLayer.Services;
using Common.DTO.User;
using DataAccessLayer.Entities;
using DataAccessLayer.Repositories.Interfaces;
using FakeItEasy;
using Xunit;

namespace BusinessLogicLayer.UnitTests
{
    public class UserServiceBehaviorTests
    {
        readonly IUserService _service;
        readonly IMapper _mapper;
        readonly IUserRepository _repository;
        public UserServiceBehaviorTests()
        {
            _mapper = A.Fake<IMapper>();
            _repository = A.Fake<IUserRepository>();
            _service = new UserService(_repository, _mapper);
        }
        [Fact]
        public void CreateUser_MustCallOnce()
        {
            _service.Create(new UserCreateDto());
            A.CallTo(() => _repository.Create(A<User>.Ignored)).MustHaveHappenedOnceExactly();
            A.CallTo(() => _repository.Save()).MustHaveHappenedOnceExactly();
        }
    }
}
