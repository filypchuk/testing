﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Common.DTO.User
{
    public sealed class UserDto
    {
        [Required]
        public int Id { get; set; }
        [Required]
        [MaxLength(32)]
        public string FirstName { get; set; }
        [Required]
        [MaxLength(32)]
        public string LastName { get; set; }
        [EmailAddress]
        public string Email { get; set; }
        public DateTime Birthday { get; set; }
        public DateTime RegisteredAt { get; set; }
        public int? TeamId { get; set; }
        public override string ToString()
        {
            return $"Id -- {Id}\n FirstName -- {FirstName}\n LastName -- {LastName}\n " +
                $"Email -- {Email}\n Birthday -- {Birthday}\n " +
                $"RegisteredAt -- {RegisteredAt}\n TeamId -- {TeamId}\n";
        }
    }
}
